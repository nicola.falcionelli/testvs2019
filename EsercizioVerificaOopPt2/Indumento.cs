﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EsercizioVerificaOopPt2
{
    class Indumento : Articolo
    {
        protected uint taglia;
        protected static int nInstances = 0;

        public Indumento(uint dim, string desc, uint t)
        {
            dimensione = dim;
            descrizione = desc;

            //sarebbe meglio con metodo set per implementare un controllo di consistenza
            taglia = t;

            nInstances++;
        }

        public override void printInformation()
        {
            Console.WriteLine("Taglia:" + taglia);
        }

        public uint getTaglia()
        {
            return taglia;
        }
    }
}
