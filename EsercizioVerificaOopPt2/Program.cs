﻿using System;

namespace EsercizioVerificaOopPt2
{
    class Program
    {
        static void Main(string[] args)
        {
            IMagazzino m1 = new MagazzinoArray(10);
            IMagazzino m2 = new MagazzinoList();

            Indumento i1 = new Indumento(2, "jeans", 46);
            Indumento i2 = new Indumento(1, "leggins", 40);
            Indumento i3 = new Indumento(3, "cargo pants", 48);

            Alimento a1 = new Alimento(2, "pennette barilla", 1675278670);
            Alimento a2 = new Alimento(1, "lenticchie", 1675278970);

            m1.inserisciArticolo(a1, 0);
            m1.inserisciArticolo(i1, 1);
            m1.inserisciArticolo(a2, 2);
            m1.filtraAlimenti(1675278000);
            m1.filtraAlimenti(1675278670);
            m1.filtraIndumenti(46);
            m1.scambiaArticoli(0, 2);
            m1.filtraAlimenti(1675278000);
            m1.filtraAlimenti(1675278670);
            m1.scambiaArticoli(1, 2);
            m1.filtraIndumenti(46);

            Console.WriteLine("----------------");

            m2.inserisciArticolo(a1, 0);
            m2.inserisciArticolo(i1, 1);
            m2.inserisciArticolo(a2, 2);
            m2.filtraAlimenti(1675278000);
            m2.filtraAlimenti(1675278670);
            m2.scambiaArticoli(0, 2);
            m2.filtraAlimenti(1675278000);
            m2.filtraAlimenti(1675278670);


        }
    }
}
