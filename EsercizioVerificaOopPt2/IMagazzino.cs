﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EsercizioVerificaOopPt2
{
    interface IMagazzino
    {
        void inserisciArticolo(Articolo a, uint i);
        void scambiaArticoli(uint i, uint j);
        void filtraAlimenti(uint valore);
        void filtraIndumenti(uint valore);

    }
}
