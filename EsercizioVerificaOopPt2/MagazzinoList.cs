﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EsercizioVerificaOopPt2
{
    class MagazzinoList : IMagazzino
    {
        protected List<Articolo> lstArticoli;
        public MagazzinoList()
        {
            lstArticoli = new List<Articolo>();
        }

        public void inserisciArticolo(Articolo a, uint i)
        {
            if ( (i+1) > lstArticoli.Count )
            {
                lstArticoli.Add(a);
            }
            else
                lstArticoli[ Convert.ToInt32(i) ] = a;
        }
        public void scambiaArticoli(uint i, uint j)
        {
            Articolo tmp = lstArticoli[Convert.ToInt32(i)];
            lstArticoli[Convert.ToInt32(i)] = lstArticoli[Convert.ToInt32(j)];
            lstArticoli[Convert.ToInt32(j)] = tmp;
        }

        public void filtraAlimenti(uint valore)
        {
            Console.WriteLine("Ecco gli alimenti con scadenza maggiore di " + valore);

            for (int i = 0; i < lstArticoli.Count; i++)
            {
                Alimento a = lstArticoli[i] as Alimento;
                if ((a != null) && (a.getTimestampScadenza() > valore))
                    Console.WriteLine("--- " + a.getDescr() + " in posizione " + i + " con scadenza " + valore);
            }
        }
        public void filtraIndumenti(uint valore)
        {
            Console.WriteLine("Ecco gli indumenti con taglia maggiore di " + valore);

            for (int i = 0; i < lstArticoli.Count; i++)
            {
                Indumento a = lstArticoli[i] as Indumento;
                if ((a != null) && (a.getTaglia() > valore))
                    Console.WriteLine("--- " + a.getDescr() + " in posizione " + i + " con taglia " + valore);
            }
        }

    }
}
