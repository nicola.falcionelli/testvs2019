﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EsercizioVerificaOopPt2
{
    class Alimento : Articolo
    {
        protected uint timestampScadenza;
        protected static int nInstances = 0;
        public Alimento(uint dim, string desc, uint tScad)
        {
            dimensione = dim;
            descrizione = desc;

            //sarebbe meglio con metodo set per implementare un controllo di consistenza
            timestampScadenza = tScad;

            nInstances++;
        }

        public override void printInformation()
        {
            Console.WriteLine("Timestamp data scadenza:" + timestampScadenza);
        }

        public uint getTimestampScadenza()
        {
            return timestampScadenza;
        }

    }
}
