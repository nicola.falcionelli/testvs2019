﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EsercizioVerificaOopPt2
{
    abstract class Articolo
    {
        protected uint dimensione;
        protected string descrizione;
        public abstract void printInformation();

        public string getDescr()
        {
            return descrizione;
        }
    }
}
