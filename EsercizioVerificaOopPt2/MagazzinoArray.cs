﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EsercizioVerificaOopPt2
{
    class MagazzinoArray : IMagazzino
    {
        protected Articolo[] arrArticoli;
        public MagazzinoArray(uint n)
        {
            arrArticoli = new Articolo[n];
        }

        public void inserisciArticolo(Articolo a, uint i)
        {
            arrArticoli[i] = a;
        }
        public void scambiaArticoli(uint i, uint j)
        {
            Articolo tmp = arrArticoli[i];
            arrArticoli[i] = arrArticoli[j];
            arrArticoli[j] = tmp;
        }

        public void filtraAlimenti(uint valore)
        {
            Console.WriteLine("Stampo gli alimenti con scadenza uguale a " + valore + "...");

            for (int i = 0; i < arrArticoli.Length; i++)
            {
                Alimento a = arrArticoli[i] as Alimento;
                if ((a != null) && (a.getTimestampScadenza() == valore))
                    Console.WriteLine(" -> " + a.getDescr() + " in posizione " + i + " con scadenza " + valore);
            }
        }
        public void filtraIndumenti(uint valore)
        {
            Console.WriteLine("Stampo gli indumenti con taglia uguale a " + valore + "...");

            for (int i = 0; i < arrArticoli.Length; i++)
            {
                Indumento a = arrArticoli[i] as Indumento;
                if ((a != null) && (a.getTaglia() == valore))
                    Console.WriteLine(" -> " + a.getDescr() + " posizione " + i + " con taglia " + valore);
            }
        }
    }
}
